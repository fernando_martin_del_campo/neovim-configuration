# Neovim config

Simple & lightweight Neovim config

## Install

1. Install vim-plug

`
pacaur -S vim-plug
`
or get vim-plug any other way.

2. Open nvim and execute

`
:PlugInstall
`

3. Install Go programming language

`
sudo pacmam -S go
`

4. Install YouCompleteMe

`
cd $HOME/.config/nvim/plugged/YouCompleteMe
`

`
python3 install.py --all
`

## License

MIT
