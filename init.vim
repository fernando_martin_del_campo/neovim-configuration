set spell spelllang=en_us
set spellfile=~/.config/nvim/spell/en.utf-8.add
set undofile
set encoding=utf-8

if has('clipboard')
  if has('unnamedplus')  " When possible use + register for copy-paste
    set clipboard=unnamed,unnamedplus
  else         " On mac and Windows, use * register for copy-paste
    set clipboard=unnamed
  endif
endif

let $NVIM_TUI_ENABLE_CURSOR_SHAPE=0

" ############################################################################################################
" Restore cursor to file position in previous editing session
"   start
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END
"   end

" ############################################################################################################
"Write date at the beginning of the line
map! <F5> <C-R>=strftime('%F (%a) %R: ')<CR>
map <F5> \|i<C-R>=strftime('%F (%a) %R: ')<CR>
" ############################################################################################################
"Make paragraph and clean spaces
map <C-c> vipgqvip:s/ $//e<CR>vip:s/  / /e<CR>
" ############################################################################################################
" change til
map <Space> ct
" ############################################################################################################
" Indenting stuff
set smartindent
set tabstop=3
set shiftwidth=3

set ignorecase
set number
set conceallevel=1
set termguicolors
set background=dark

" ############################################################################################################
" Setup dictionaries for tab completion
au FileType * exec("setlocal dictionary+=".$HOME."/.config/nvim/dictionaries/".expand('<amatch>'))
set complete+=k
" ############################################################################################################
" F11 for showing recent documents
map <F11> :bro old

" F10 for showing unsaved files
map <F10> :recover

" ############################################################################################################
" Change default symbol in splits
set fillchars=|
" ############################################################################################################

set expandtab
set autoindent
set softtabstop=4
set shiftwidth=2
set tabstop=4

set history=1000

" Visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv

" Allow using the repeat operator with a visual selection (!)
" http://stackoverflow.com/a/8064607/127816
vnoremap . :normal .<CR>

colorscheme solarized

autocmd BufEnter * lcd %:p:h

filetype plugin indent on

set undodir=~/.config/nvim/undodir

call plug#begin()

Plug 'vim-scripts/Vimball'
Plug 'godlygeek/tabular'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/goyo.vim'
Plug 'rust-lang/rust.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'groenewege/vim-less'
Plug 'tpope/vim-markdown'
Plug 'vim-scripts/nginx.vim'
Plug 'Valloric/YouCompleteMe'
Plug 'michaeljsmith/vim-indent-object'
Plug 'vim-syntastic/syntastic'
Plug 'Shougo/neocomplcache.vim'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-gitgutter'
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'mxw/vim-jsx', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'Raimondi/delimitMate'
Plug 'tmhedberg/SimpylFold'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'mhinz/vim-startify'
Plug 'mhinz/neovim-remote'
Plug 'vim-scripts/nginx.vim'
Plug 'leafgarland/typescript-vim'
Plug 'Quramy/tsuquyomi'
Plug 'Shougo/vimproc.vim'
Plug 'lervag/vimtex'

call plug#end()

let g:javascript_plugin_jsdoc           = 1
let g:javascript_conceal_function       = "ƒ"
let g:javascript_conceal_null           = "ø"
let g:javascript_conceal_arrow_function = "⇒"
let g:javascript_conceal_return         = "⇚"

let g:jsx_ext_required = 0

let g:syntastic_check_on_open=1

map <C-E> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen=1
let NERDTreeShowHidden=1
let NERDTreeIgnore = ['\.pyc$']

let g:ycm_autoclose_preview_window_after_completion=1

set foldmethod=indent
set foldlevel=99

au BufRead,BufNewFile *.nginx set ft=nginx
au BufRead,BufNewFile */etc/nginx/* set ft=nginx
au BufRead,BufNewFile */usr/local/nginx/conf/* set ft=nginx
au BufRead,BufNewFile nginx.conf set ft=nginx

" #################################################
let g:tex_flavor  = 'latex'

let g:tex_conceal = ''

let g:vimtex_fold_manual = 1

let g:vimtex_latexmk_continuous = 1

let g:vimtex_compiler_progname = 'nvr'

let g:vimtex_view_method = 'mupdf'
" #################################################

set mousehide
